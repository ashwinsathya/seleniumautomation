import com.aventstack.extentreports.Status;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.v96.network.Network;
import org.openqa.selenium.devtools.v96.fetch.Fetch;
import org.openqa.selenium.devtools.v96.network.model.RequestId;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.Optional;

public class Selenium4DevTools {

    private static ChromeDriver driver;
    private static DevTools chromeDevTools;


    public static void main(String[] args) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

           /* chromeDevTools.send(Fetch.enable(Optional.empty(),Optional.empty()));
            chromeDevTools.addListener(Fetch.requestPaused(),request -> {
                boolean url =request.getRequest().getUrl().contains("ui");
                if(url) System.out.println(request.getResponseStatusText());

                    }

            );
            chromeDevTools.send(Network.enable(Optional.empty(), Optional.empty(), Optional.empty()));
            chromeDevTools.addListener(Network.requestWillBeSent(),
                    entry -> {
                        System.out.println("Request URI : " + entry.getRequest().getUrl()+"\n"
                                + " With method : "+entry.getRequest().getMethod() + "\n");
                        entry.getRequest().getMethod();
                    });*/
        // driver.get("https://rahulshettyacademy.com/angularAppdemo");
        //driver.findElement(By.xpath("//button[text()=' Virtual Library ']")).click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(30));
        driver.manage().window().maximize();
        driver.get("https://qa.sales.firstdata.com/#/");
        clickonElement(driver.findElement(By.id("login-button")));

        enterValues(driver.findElement(By.id("username")), "Mrktest01");
        enterValues(driver.findElement(By.id("password")), "Dec212021");
        clickonElement(driver.findElement(By.xpath("//a[@class='btn submit']")));

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        //ExtentTestManager.getTest().log(Status.INFO, "Logged In Successfully");
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@id='sales-toggle']/span[text()='Open']")));
        Thread.sleep(1000);
        clickonElement(driver.findElement(By.id("sales-toggle")));
        //  ExtentTestManager.getTest().log(Status.INFO, "Creating Lead");
        clickonElement(driver.findElement(By.xpath("//a[text()='Create Lead']")));
        enterValues(driver.findElement(By.name("zip")), "12345");
        enterValues(driver.findElement(By.name("company")), "MMIS TEST 31JAN2022 GFT");
        enterValues(driver.findElement(By.name("name")), "JOHN DOE");
        enterValues(driver.findElement(By.name("name")), "JOHN DOE");
        enterValues(driver.findElement(By.name("phone")), "4655468897");
        enterValues(driver.findElement(By.name("email")), "ashwin.sathyanarayanan@fiserv.com");
        enterValues(driver.findElement(By.name("address1")), "10,AVE ST");
        enterValues(driver.findElement(By.name("vol")), "9000");
        Thread.sleep(500);
        //Agent
        enterValues(driver.findElement(By.id("bank")), "984971355883");


        clickonElement(driver.findElement(By.xpath("//li[text()='EMPS OA SPECIAL REF PROG/EMPS OA SPECIAL REF PROG/984971355883']")));
        clickonElement(driver.findElement(By.xpath("//a[text()='Create']")));
        // ExtentTestManager.getTest().log(Status.INFO, "Lead Created successfully");
        System.out.println("Status is::" + getText(driver.findElement(By.cssSelector("#selectToBegin h3.float-left"))));
        clickonElement(driver.findElement(By.xpath("//a[text()='Shop Products']")));
        //String scr2 =  ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);

        //ExtentTestManager.getTest().log(Status.INFO,"Created lead successfully",ExtentTestManager.getTest().addScreenCaptureFromBase64String("data:image/png;base64,"+scr2).getModel().getMedia().get(0));
        // attachbase64Img("Created lead successfully");
        clickonElement(driver.findElement(By.id("sales-toggle")));
        wait.until(ExpectedConditions.urlContains("/home#shop-businesstypes"));
        clickonElement(driver.findElement(By.xpath("//a/span[text()='Retail']")));
        wait.until(ExpectedConditions.urlContains("home#shop-businesstypes"));
        clickonElement(driver.findElement(By.linkText("RETAIL (KEY ENTRY)")));
        // ngDriver.waitForAngularRequestsToFinish();
        //  ExtentTestManager.getTest().log(Status.INFO, "Retail -> Retail Key Entry is selected");
        Thread.sleep(1000);
        wait.until(ExpectedConditions.urlContains("products/c#shop-businesstypes"));
        clickonElement(driver.findElement(By.xpath("//p[text()='Clover Flex 2nd Gen']/following-sibling::a")));
        Thread.sleep(2000);
        // ExtentTestManager.getTest().log(Status.INFO, "Selected Clover Flex 2ndGen Purchase");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.breadcrumb-inner.container > ul>li:nth-of-type(3)")));
        clickonElement(driver.findElement(By.xpath("//div[@id='product-info']//a[text()='Subscribe']")));

        WebElement recurringqty = driver.findElement(By.xpath("//table[@id='recurring-fees']//select[@ng-model='p.qty']"));

        selectByVisibleText(recurringqty, "1");
        Thread.sleep(2000);
        clickonElement(driver.findElement(By.id("add-to-cart")));
        WebElement cartqty = driver.findElement(By.xpath("//table[@id='cart-items']//select[@ng-model='p.qty']"));

        selectByVisibleText(cartqty, "1");
        Thread.sleep(2000);
        selectProduct("FD150");
        selectProduct("Payeezy Hosted Checkout (Retail/Moto)");
        Thread.sleep(3000);
//        ExtentTestManager.getTest().log(Status.INFO, "Selected Payeezy Purchase");
        selectProduct("4D Payments Software (Payment Connection)");
        //ExtentTestManager.getTest().log(Status.INFO, "Selected 4D Payments Software (Payment Connection) Purchase");

        //Browse products
        clickUsingJs(driver.findElement(By.xpath("//p[text()='Security Protection for Payeezy']/following-sibling::div/a[text()='Browse Products']")));

        clickonElement(driver.findElement(By.xpath("//p[text()='Clover Security Plus (for Payeezy)']/following-sibling::a")));

        Thread.sleep(2000);
       // ExtentTestManager.getTest().log(Status.INFO, "Security Protection purchased for Payeezy - Clovery Security Plus");

        clickonElement(driver.findElement(By.xpath("//p[text()='Security Protection for Var']/following-sibling::div/a[text()='Browse Products']")));

        clickonElement(driver.findElement(By.xpath("//p[text()='Clover Security Plus (for Clover Go and VARS)']/following-sibling::a")));
        Thread.sleep(2000);
      //  ExtentTestManager.getTest().log(Status.INFO, "Security Protection purchased for Vars - Clovery Security Plus VARS");

        //ngDriver.waitForAngularRequestsToFinish();
        //  String scrShot3 =  "data:image/png;base64," + ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);

        //ExtentTestManager.getTest().log(Status.INFO,"Added Security and Var products successfully",ExtentTestManager.getTest().addScreenCaptureFromBase64String(scrShot3).getModel().getMedia().get(0));
        //attachbase64Img("Added Security and Var products successfully");
        clickonElement(driver.findElement(By.xpath("//a[text()='Browse Products']")));
        Thread.sleep(2000);
        //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='Early Termination Fee']/following-sibling::a[@ng-click='addToCart(p)']")));
        clickonElement(driver.findElement(By.xpath("//p[text()='Early Termination Fee']/following-sibling::a[@ng-click='addToCart(p)']")));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@id='cart-items']/tbody/tr/td/div/p/a[text()='Early Termination Fee']")));
        //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='Browse Products']")));
        Thread.sleep(2000);
       // ExtentTestManager.getTest().log(Status.INFO, "Early Termination Fee Purchased");
        // ngDriver.waitForAngularRequestsToFinish();
        clickonElement(driver.findElement(By.xpath("//a[text()='Browse Products']")));
        Thread.sleep(1000);
        wait.until(ExpectedConditions.urlContains("/options/Tiers"));
        Thread.sleep(1000);
        clickonElement(driver.findElement(By.xpath("//p[contains(text(),'Global Fee Table')]/following-sibling::a")));
       // ExtentTestManager.getTest().log(Status.INFO, "GFT Purchased");
        wait.until(ExpectedConditions.urlContains("/processing"));
        addPaymentTypes("Debit (GFT)");
        addPaymentTypes("Discover GFT");
        addPaymentTypes("Visa/Mastercard GFT");
        addPaymentTypes("American Express Opt Blue GFT");
        Thread.sleep(500);
        addPaymentTypes("Debit (GFT)");
      //  ExtentTestManager.getTest().log(Status.INFO, "GFT - Debit,Discover,Visa/masterCard,American Express purchased");


        //addPaymentTypes("Visa/Mastercard GFT");
        Thread.sleep(2000);

        clickUsingJs(driver.findElement(By.xpath("//div[@id='cart-actions']/a[not (contains(@class,'disabled') )and text()='PROCEED TO CHECKOUT']")));
        wait.until(ExpectedConditions.textToBe(By.xpath("//div[@id='opportunity-cart']/h2"), "Store Locations"));
        clickonElement(driver.findElement(By.xpath("//div[@id='opportunity-cart']/div/a[text()='5']")));
        clickonElement(driver.findElement(By.xpath("//div[@id='cart-actions']/a[text()='Next']")));
        //Transaction Info page
        wait.until(ExpectedConditions.urlContains("transaction/info"));

        selectByVisibleText(driver.findElement(By.id("mcccodes")), "Appliances, Electronics, Computers");
        //wait.until(ExpectedConditions.elementToBeClickable(By.id("mcctypes")));
        selectByVisibleText(driver.findElement(By.id("mcctypes")), "Camera and Photo Supply");
        enterValues(driver.findElement(By.name("sales")), "1000000");
        enterValues(driver.findElement(By.name("annualcardVolume")), "1000000");
        enterValues(driver.findElement(By.name("ticket")), "500");
        enterValues(driver.findElement(By.id("amexVolume")), "10000");
        Thread.sleep(500);

        enterValues(driver.findElement(By.id("GFTNumber")), "880000000123" + Keys.TAB);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("p[ng-if='transactionFormData.globalFeeDetails.globalFeeTableDescription']")));

        clickUsingJs(driver.findElement(By.xpath("//div[@id='cart-actions']/a[text()='Next' and not(contains(@class,'disabled'))]")));
        //enterValues(driver.findElement(By.name("prodprice")),"450" );
        //Thread.sleep(2000);
        wait.until(ExpectedConditions.urlContains("/shipping"));
        //editPricesforProfitablityError();
        //Thread.sleep(1000);
        //enterPromotions("96P 500 Statement Cred Over 6 Months");
        /*
        chromeDevTools = driver.getDevTools();
        chromeDevTools.createSession();
        chromeDevTools.send(Network.enable(Optional.empty(), Optional.empty(), Optional.empty()));
        //chromeDevTools.addListener(Fetch);
        chromeDevTools.addListener(Network.requestWillBeSent(), request -> {
           // if(request.getRequest().getUrl().equals("https://qa.sales.firstdata.com/v1/merchantorders"));
            System.out.println("Request is ::"+request.getRequest().getUrl());
        });
        final RequestId[] requestId = new RequestId[1];
        chromeDevTools.addListener(Network.responseReceived(), response -> {
            //org.openqa.selenium.devtools.v96.network.model.RequestId requestId = response.getRequestId();
            requestId[0] =response.getRequestId();
            System.out.println("Response is"+response.getResponse().getHeaders().toString());
            System.out.println("Response Url => " + response.getResponse().getUrl());

            System.out.println("Response Status => " + response.getResponse().getStatus());

            System.out.println("Response Headers => " + response.getResponse().getHeaders().toString());

            System.out.println("Response MIME Type => " + response.getResponse().getMimeType().toString());

            System.out.println("------------------------------------------------------");

        });*/
        DevTools devTools = ((ChromeDriver) driver).getDevTools();
        devTools.createSession();
        devTools.send(Network.clearBrowserCache());
        devTools.send(Network.setCacheDisabled(true));

        final RequestId[] requestIds = new RequestId[1];
        devTools.send(Network.enable(Optional.empty(), Optional.empty(), Optional.of(100000000)));
        devTools.addListener(Network.responseReceived(), responseReceived -> {


            requestIds[0] = responseReceived.getRequestId();
            String url = responseReceived.getResponse().getUrl();
            //System.out.println("Url:: "+url);
            int status = responseReceived.getResponse().getStatus();
           // System.out.println("Status:: "+status);
            String type = responseReceived.getType().toJson();
            //System.out.println("Type:: "+type);
            String headers = responseReceived.getResponse().getHeaders().toString();
            //System.out.println("Headers:: "+headers);
            String responseBody = devTools.send(Network.getResponseBody(requestIds[0])).getBody();
            if(url.equals("https://qa.sales.firstdata.com/v1/merchantorders"))
            System.out.println("Response ::: "+responseBody.replace("{\"orderId\":\"","").replace("\"}",""));
        });

        /*chromeDevTools.addListener(Network.responseReceived(), request -> {
            System.out.println(request.getResponse().getStatus());
        });*/
        clickonElement(driver.findElement(By.xpath("//div[@id='proposal']/div/a[text()='Place Order']")));
        wait.until(ExpectedConditions.urlContains("/thankyou"));
        devTools.send(Network.disable());
        // driver.get("https://qa.sales.firstdata.com/#/signup/terms/PRA4K/APE67");
     //   chromeDevTools.send(Network.disable());
           /* chromeDevTools = driver.getDevTools();
            chromeDevTools.createSession();
            chromeDevTools.send(Network.enable(Optional.empty(),Optional.empty(),Optional.empty()));

            chromeDevTools.send(Fetch.enable(Optional.empty(),Optional.empty()));
            chromeDevTools.addListener(Network.requestIntercepted(),request -> {
                System.out.println("Request2 URI : " + request.getRequest().getUrl()+"\n"
                        + " With method2 : "+request.getRequest().getMethod() + "\n");
                System.out.println("METHODDDD::::  "+request.getRequest().getMethod());
                System.out.println("Resonse is"+request.getResponseStatusCode());
                        boolean url =request.getRequest().getUrl().contains("events");
                        if(url) {System.out.println("Resonse is"+request.getResponseStatusCode());}

                    }

            );*/
        //driver.findElement(By.id("shellmenu_1")).click();
    }

    private static void clickonElement(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();


    }

    private static void enterValues(WebElement element, String value) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.clear();
        element.sendKeys(value);
    }

    private static String getText(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));

        return element.getText();
    }

    private static void selectByVisibleText(WebElement element, String value) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        Select select = new Select(element);
        select.selectByVisibleText(value);
    }

    private static void clickUsingJs(WebElement element) {

        JavascriptExecutor js = (JavascriptExecutor) driver;


        js.executeScript("arguments[0].click();", element);
    }

    private static void selectProduct(String product) throws InterruptedException {
        Thread.sleep(200);
        clickonElement(driver.findElement(By.xpath("//div[@id='cart-actions']/div/a[text()='Continue Shopping']")));
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        enterValues(driver.findElement(By.id("search-products")), product);
        Thread.sleep(2000);
        String xpath = "//p[text()='" + product + "']/following-sibling::a[text()='View Details']";
        clickonElement(driver.findElement(By.xpath(xpath)));
        Thread.sleep(3000);
        clickUsingJs(driver.findElement(By.id("add-to-cart")));
    }

    private static void addPaymentTypes(String paymenttype) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        String ele = "//p[text()='" + paymenttype + "']/following-sibling::a[text()='Add to Cart']";
        WebElement element = driver.findElement(By.xpath(ele));
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public static void editPricesforProfitablityError() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//tr[@ng-repeat='p in productPricing track by $index']/td//input[not(contains(@disabled,'disabled'))]"));
        List<WebElement> products = driver.findElements(By.xpath("//tr[@ng-repeat='p in productPricing track by $index']/td/ng-form/input[not(contains(@disabled,'disabled'))]/ancestor::td/preceding-sibling::td"));
        for (int i = 0; i < elements.size(); i++) {
            String originalPrice = elements.get(i).getAttribute("value");
            String minPrice = elements.get(i).getAttribute("abs-min");
            String maxPrice = elements.get(i).getAttribute("abs-max");
            enterValues(elements.get(i), String.valueOf(Float.parseFloat(minPrice) - 1));
            String error = getAttribute(driver.findElement(By.xpath("//tr[@ng-repeat='p in productPricing track by $index']/td/i")), "class");
            if (error.equalsIgnoreCase("absMinError error-indicator fa fa-lg fa-exclamation-circle ng-scope"))
                // System.out.println("Error Message matches for Minimum value");
                //ExtentTestManager.getTest().log(Status.INFO, "Error Message matches for Minimum value" + products.get(i).getText());
            Thread.sleep(500);
            enterValues(elements.get(i), String.valueOf(Float.parseFloat(maxPrice) + 1));
            error = getAttribute(driver.findElement(By.xpath("//tr[@ng-repeat='p in productPricing track by $index']/td/i")), "class");
            if (error.equalsIgnoreCase("absMaxError error-indicator fa fa-lg fa-exclamation-circle ng-scope"))
              //  ExtentTestManager.getTest().log(Status.INFO, "Error Message matches for Maximum value" + products.get(i).getText());
            // System.out.println("Error Message matches for Maximum value");
            Thread.sleep(200);
            enterValues(elements.get(i), originalPrice);

        }
    }
        private static void enterPromotions (String promotion) throws InterruptedException {
            // By showDetail = RelativeLocator.with(By.xpath("//a[text()='Show Detail']")).toRightOf(By.xpath("//h3[text()='Promotions']"));
            JavascriptExecutor js = (JavascriptExecutor) driver;
            By showDetail = By.xpath("//h3[text()='Promotions']/ancestor::tr/th/a[text()='Show Detail']");
            /*clickonElement(driver.findElement(showDetail));
            Actions actions = new Actions(driver);
            actions.sendKeys(Keys.PAGE_DOWN);
            actions.moveToElement(driver.findElement(showDetail)).click();*/

            js.executeScript("arguments[0].click();", driver.findElement(showDetail));
            String xpath = "//td[text()='" + promotion + "']/following-sibling::td/a[text()=' Apply']";
            By applyPromotion = By.xpath(xpath);
            //By applyPromotion = RelativeLocator.with(By.xpath("//a[text()=' Apply']")).toRightOf(By.xpath(xpath));
            Thread.sleep(2000);
            js.executeScript("arguments[0].click();", driver.findElement(applyPromotion));
            //clickonElement(driver.findElement(applyPromotion));

            try {
                xpath = "//td[text()='" + promotion + "']/following-sibling::td/a[text()=' Remove']";
                WebElement element = driver.findElement(By.xpath(xpath));
                if (!element.isDisplayed()) {

                   // ExtentTestManager.getTest().log(Status.WARNING, "Promotion provided is not valid to apply" + promotion);
                } else {
                   // ExtentTestManager.getTest().log(Status.INFO, "Promotion provided is valid to apply" + promotion);
                }
            } catch (Exception e) {
                //ExtentTestManager.getTest().log(Status.WARNING, "Promotion provided is not applied :: " + promotion);
            }
        }
        private static String getAttribute (WebElement element, String attr){
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            wait.until(ExpectedConditions.elementToBeClickable(element));
            return element.getAttribute(attr);
        }
    }



