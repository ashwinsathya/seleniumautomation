import com.aventstack.extentreports.Status;
import com.paulhammant.ngwebdriver.NgWebDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.checkerframework.checker.units.qual.A;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.chromium.HasCdp;
import org.openqa.selenium.devtools.DevTools;
//import org.openqa.selenium.devtools.HasDevTools;
import org.openqa.selenium.devtools.HasDevTools;
import org.openqa.selenium.devtools.v96.log.Log;
import org.openqa.selenium.devtools.v96.network.Network;
import org.openqa.selenium.devtools.v96.network.model.RequestId;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import java.net.URL;
import java.time.Duration;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class GFTFlow {

        public static WebDriver driver;
        //public static WebDriver driver;
        public static final String URL = "https://sso-fdc-qa-Krushna.Padhy:81ee4bc2-646a-4c4e-9b79-47615678d3c5@ondemand.saucelabs.com:443/wd/hub";
        public static void main (String[]args){

            try {
                WebDriverManager.chromedriver().setup();
                ChromeOptions browserOptions = new ChromeOptions();
                browserOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);

                browserOptions.setPlatformName("Windows 10");
                browserOptions.setBrowserVersion("latest");

                Map<String, Object> sauceOptions = new HashMap<>();
                //sauceOptions.put("screenResolution", "1024x768");
                //sauceOptions.put("username", System.getenv("SAUCE_USERNAME"));
                //sauceOptions.put("accessKey", System.getenv("SAUCE_ACCESS_KEY"));
               browserOptions.setCapability("sauce:options", sauceOptions);
                //driver = new ChromeDriver(browserOptions);
                //DevTools devTools = driver.getDevTools();


                URL url = new URL("https://sso-fdc-qa-Krushna.Padhy:81ee4bc2-646a-4c4e-9b79-47615678d3c5@ondemand.us-west-1.saucelabs.com:443/wd/hub");

                driver = new RemoteWebDriver(url, browserOptions);


                //ngDriver.waitForAngularRequestsToFinish();
                driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
                driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(30));
                driver.manage().window().maximize();

                ExtentTestManager.startTest("Sales Create lead flow", "Firstdata sales create lead flow");
                driver.get("https://qa.sales.firstdata.com/#/");
                clickonElement(driver.findElement(By.id("login-button")));

                enterValues(driver.findElement(By.id("username")), "Mrktest01");
                enterValues(driver.findElement(By.id("password")), "Dec212021");
                clickonElement(driver.findElement(By.xpath("//a[@class='btn submit']")));

                WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
                ExtentTestManager.getTest().log(Status.INFO, "Logged In Successfully");
                attachbase64Img("Logged in successfully");
                //String scrShot =  "data:image/png;base64," + ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);

               // ExtentTestManager.getTest().log(Status.INFO,"Logged in successfully",ExtentTestManager.getTest().addScreenCaptureFromBase64String(scrShot).getModel().getMedia().get(0));
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@id='sales-toggle']/span[text()='Open']")));
                Thread.sleep(1000);
                clickonElement(driver.findElement(By.id("sales-toggle")));
                ExtentTestManager.getTest().log(Status.INFO, "Creating Lead");
                clickonElement(driver.findElement(By.xpath("//a[text()='Create Lead']")));
                enterValues(driver.findElement(By.name("zip")), "12345");
                enterValues(driver.findElement(By.name("company")), "MMIS TEST 31JAN2022 GFT");
                enterValues(driver.findElement(By.name("name")), "JOHN DOE");
                enterValues(driver.findElement(By.name("name")), "JOHN DOE");
                enterValues(driver.findElement(By.name("phone")), "4655468897");
                enterValues(driver.findElement(By.name("email")), "ashwin.sathyanarayanan@fiserv.com");
                enterValues(driver.findElement(By.name("address1")), "10,AVE ST");
                enterValues(driver.findElement(By.name("vol")), "9000");
                Thread.sleep(500);
                //Agent
                enterValues(driver.findElement(By.id("bank")), "984971355883");



                clickonElement(driver.findElement(By.xpath("//li[text()='EMPS OA SPECIAL REF PROG/EMPS OA SPECIAL REF PROG/984971355883']")));
                clickonElement(driver.findElement(By.xpath("//a[text()='Create']")));
                ExtentTestManager.getTest().log(Status.INFO, "Lead Created successfully");
                System.out.println("Status is::" + getText(driver.findElement(By.cssSelector("#selectToBegin h3.float-left"))));
                clickonElement(driver.findElement(By.xpath("//a[text()='Shop Products']")));
                //String scr2 =  ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);

                //ExtentTestManager.getTest().log(Status.INFO,"Created lead successfully",ExtentTestManager.getTest().addScreenCaptureFromBase64String("data:image/png;base64,"+scr2).getModel().getMedia().get(0));
                attachbase64Img("Created lead successfully");
                clickonElement(driver.findElement(By.id("sales-toggle")));
                wait.until(ExpectedConditions.urlContains("/home#shop-businesstypes"));
                clickonElement(driver.findElement(By.xpath("//a/span[text()='Retail']")));
                wait.until(ExpectedConditions.urlContains("home#shop-businesstypes"));
                clickonElement(driver.findElement(By.linkText("RETAIL (KEY ENTRY)")));
                // ngDriver.waitForAngularRequestsToFinish();
                ExtentTestManager.getTest().log(Status.INFO, "Retail -> Retail Key Entry is selected");
                Thread.sleep(1000);
                wait.until(ExpectedConditions.urlContains("products/c#shop-businesstypes"));
                clickonElement(driver.findElement(By.xpath("//p[text()='Clover Flex 2nd Gen']/following-sibling::a")));
                Thread.sleep(2000);
                ExtentTestManager.getTest().log(Status.INFO, "Selected Clover Flex 2ndGen Purchase");
                wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.breadcrumb-inner.container > ul>li:nth-of-type(3)")));
                clickonElement(driver.findElement(By.xpath("//div[@id='product-info']//a[text()='Subscribe']")));

                WebElement recurringqty = driver.findElement(By.xpath("//table[@id='recurring-fees']//select[@ng-model='p.qty']"));

                selectByVisibleText(recurringqty, "1");
                Thread.sleep(2000);
                clickonElement(driver.findElement(By.id("add-to-cart")));
                WebElement cartqty = driver.findElement(By.xpath("//table[@id='cart-items']//select[@ng-model='p.qty']"));

                selectByVisibleText(cartqty, "1");
                Thread.sleep(2000);
                ExtentTestManager.getTest().log(Status.INFO, "Subscribed Clover Flex 2ndGen Purchase");
                ExtentTestManager.getTest().log(Status.INFO, "Selected Clover Flex 2ndGen Purchase");
               //String scrShot2 =  "data:image/png;base64," + ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);

                //ExtentTestManager.getTest().log(Status.INFO,"Purchased and subscribed Products",ExtentTestManager.getTest().addScreenCaptureFromBase64String(scrShot2).getModel().getMedia().get(0));
                attachbase64Img("Purchased and subscribed Products");
                //Add multiple Products
                selectProduct("FD150");
                ExtentTestManager.getTest().log(Status.INFO, "Selected FD150 Purchase");
                selectProduct("Payeezy Hosted Checkout (Retail/Moto)");
                Thread.sleep(3000);
                ExtentTestManager.getTest().log(Status.INFO, "Selected Payeezy Purchase");
                selectProduct("4D Payments Software (Payment Connection)");
                ExtentTestManager.getTest().log(Status.INFO, "Selected 4D Payments Software (Payment Connection) Purchase");

                //Browse products
                clickUsingJs(driver.findElement(By.xpath("//p[text()='Security Protection for Payeezy']/following-sibling::div/a[text()='Browse Products']")));

                clickonElement(driver.findElement(By.xpath("//p[text()='Clover Security Plus (for Payeezy)']/following-sibling::a")));

                Thread.sleep(2000);
                ExtentTestManager.getTest().log(Status.INFO, "Security Protection purchased for Payeezy - Clovery Security Plus");

                clickonElement(driver.findElement(By.xpath("//p[text()='Security Protection for Var']/following-sibling::div/a[text()='Browse Products']")));

                clickonElement(driver.findElement(By.xpath("//p[text()='Clover Security Plus (for Clover Go and VARS)']/following-sibling::a")));
                Thread.sleep(2000);
                ExtentTestManager.getTest().log(Status.INFO, "Security Protection purchased for Vars - Clovery Security Plus VARS");

                //ngDriver.waitForAngularRequestsToFinish();
              //  String scrShot3 =  "data:image/png;base64," + ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);

                //ExtentTestManager.getTest().log(Status.INFO,"Added Security and Var products successfully",ExtentTestManager.getTest().addScreenCaptureFromBase64String(scrShot3).getModel().getMedia().get(0));
                attachbase64Img("Added Security and Var products successfully");
                clickonElement(driver.findElement(By.xpath("//a[text()='Browse Products']")));
                Thread.sleep(2000);
                //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='Early Termination Fee']/following-sibling::a[@ng-click='addToCart(p)']")));
                clickonElement(driver.findElement(By.xpath("//p[text()='Early Termination Fee']/following-sibling::a[@ng-click='addToCart(p)']")));
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@id='cart-items']/tbody/tr/td/div/p/a[text()='Early Termination Fee']")));
                //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='Browse Products']")));
                Thread.sleep(2000);
                ExtentTestManager.getTest().log(Status.INFO, "Early Termination Fee Purchased");
                // ngDriver.waitForAngularRequestsToFinish();
                clickonElement(driver.findElement(By.xpath("//a[text()='Browse Products']")));
                Thread.sleep(1000);
                wait.until(ExpectedConditions.urlContains("/options/Tiers"));
                Thread.sleep(1000);
                clickonElement(driver.findElement(By.xpath("//p[contains(text(),'Global Fee Table')]/following-sibling::a")));
                ExtentTestManager.getTest().log(Status.INFO, "GFT Purchased");
                wait.until(ExpectedConditions.urlContains("/processing"));
                addPaymentTypes("Debit (GFT)");
                addPaymentTypes("Discover GFT");
                addPaymentTypes("Visa/Mastercard GFT");
                addPaymentTypes("American Express Opt Blue GFT");
                Thread.sleep(500);
                addPaymentTypes("Debit (GFT)");
                ExtentTestManager.getTest().log(Status.INFO, "GFT - Debit,Discover,Visa/masterCard,American Express purchased");


                //addPaymentTypes("Visa/Mastercard GFT");
                Thread.sleep(2000);

                clickUsingJs(driver.findElement(By.xpath("//div[@id='cart-actions']/a[not (contains(@class,'disabled') )and text()='PROCEED TO CHECKOUT']")));
                wait.until(ExpectedConditions.textToBe(By.xpath("//div[@id='opportunity-cart']/h2"),"Store Locations"));
                clickonElement(driver.findElement(By.xpath("//div[@id='opportunity-cart']/div/a[text()='5']")));
                clickonElement(driver.findElement(By.xpath("//div[@id='cart-actions']/a[text()='Next']")));
                //Transaction Info page
                wait.until(ExpectedConditions.urlContains("transaction/info"));

                selectByVisibleText(driver.findElement(By.id("mcccodes")), "Appliances, Electronics, Computers");
                //wait.until(ExpectedConditions.elementToBeClickable(By.id("mcctypes")));
                selectByVisibleText(driver.findElement(By.id("mcctypes")), "Camera and Photo Supply");
                enterValues(driver.findElement(By.name("sales")),"1000000");
                enterValues(driver.findElement(By.name("annualcardVolume")),"1000000");
                enterValues(driver.findElement(By.name("ticket")),"500");
                enterValues(driver.findElement(By.id("amexVolume")),"10000");
                Thread.sleep(500);

                enterValues(driver.findElement(By.id("GFTNumber")), "880000000123"+Keys.TAB);
               wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("p[ng-if='transactionFormData.globalFeeDetails.globalFeeTableDescription']")));

                clickUsingJs(driver.findElement(By.xpath("//div[@id='cart-actions']/a[text()='Next' and not(contains(@class,'disabled'))]")));
                //enterValues(driver.findElement(By.name("prodprice")),"450" );
                //Thread.sleep(2000);
                wait.until(ExpectedConditions.urlContains("/shipping"));
                editPricesforProfitablityError();
                Thread.sleep(1000);
                enterPromotions("96P 500 Statement Cred Over 6 Months");
                ExtentTestManager.getTest().log(Status.INFO,"Order ID is :: "+getOrderId());
                clickonElement(driver.findElement(By.xpath("//div[@id='proposal']/div/a[text()='Place Order']")));
                wait.until(ExpectedConditions.urlContains("/thankyou"));

                clickonElement(driver.findElement(By.xpath("//a[text()='Sign Up']")));
                wait.until(ExpectedConditions.urlContains("/signup/owner"));
                //Owner & Business Info
                // Thread.sleep(2000);


                selectByVisibleText(driver.findElement(By.name("TAX_FILING_NAME_SAME_AS_BUSINESS_LEGAL_NAME")),"Yes" );

                selectByVisibleText(driver.findElement(By.id("files-taxes")),"By owner's Social Security Number" );

                selectByVisibleText(driver.findElement(By.name("YEAR_BUSINESS_STARTED")),"2022" );

                selectByVisibleText(driver.findElement(By.name("MONTH_BUSINESS_STARTED")),"January" );

                selectByVisibleText(driver.findElement(By.name("INCORPORATION_STATE")),"Alabama" );

                selectByVisibleText(driver.findElement(By.name("ORGANIZATION_TYPE")),"Partnerships" );

                enterValues(driver.findElement(By.name("EIN")),"546545646");

                selectByVisibleText(driver.findElement(By.name("FOREIGN_OWNERSHIP")),"None" );

                //Partner1
                enterValues(driver.findElement(By.id("name_0")),"KENNETH CLINTMORE" );
                selectByVisibleText(driver.findElement(By.name("title1_0")),"Partner" );
                enterValues(driver.findElement(By.name("phone_0")),"4029559455");
                enterValues(driver.findElement(By.name("mobilePhone_0")),"4029559455");
                selectByVisibleText(driver.findElement(By.name("dob_month_0")),"August" );

                selectByVisibleText(driver.findElement(By.name("dob_day_0")),"27" );

                selectByVisibleText(driver.findElement(By.name("dob_year_0")),"1964" );
                Thread.sleep(500);
                enterValues(driver.findElement(By.name("email_0")),"santhosh.rao.m@gmail.com");
                enterValues(driver.findElement(By.name("email2_0")),"santhosh.rao.m@gmail.com");


                enterValues(driver.findElement(By.name("Address1_0")),"784 GREENILLE RD");

                enterValues(driver.findElement(By.name("zip_0")),"10065");
                enterValues(driver.findElement(By.name("city_0")),"LEDFOREST");
                selectByVisibleText(driver.findElement(By.name("state_0")),"Michigan");
                enterValues(driver.findElement(By.name("SocialSecurityNumber_0")),"5145");
                enterValues(driver.findElement(By.name("percentOwned_0")),"50");
                clickonElement(driver.findElement(By.cssSelector(".link.add-owner1")));

                /*
                selectByVisibleText(driver.findElement(By.name("title1_0")),"Partner" );
                enterValues(driver.findElement(By.name("phone_0")),"3465465465");


                enterValues(driver.findElement(By.name("mobilePhone_0")),"4565465465");

                selectByVisibleText(driver.findElement(By.name("dob_month_0")),"January" );

                selectByVisibleText(driver.findElement(By.name("dob_day_0")),"3" );

                selectByVisibleText(driver.findElement(By.name("dob_year_0")),"1989" );
                enterValues(driver.findElement(By.name("email2_0")),"ashwin.sathyanarayanan@fiserv.com");

                enterValues(driver.findElement(By.name("SocialSecurityNumber_0")),"354654664");

                enterValues(driver.findElement(By.name("Address1_0")),"12, AVE ST");

                enterValues(driver.findElement(By.name("zip_0")),"12345");

                enterValues(driver.findElement(By.name("percentOwned_0")),"50");
                clickonElement(driver.findElement(By.cssSelector(".link.add-owner1")));
    */
                //Owner 2

                enterValues(driver.findElement(By.name("name_1")),"JOHN AA");

                //selectByVisibleText(driver.findElement(By.name("title1_1")),"Partner" );

                enterValues(driver.findElement(By.name("phone_1")),"3465965465");

                enterValues(driver.findElement(By.name("phone_1")),"4565065465");

                selectByVisibleText(driver.findElement(By.name("dob_month_1")),"February" );

                selectByVisibleText(driver.findElement(By.name("dob_day_1")),"3" );

                selectByVisibleText(driver.findElement(By.name("dob_year_1")),"1988" );


                enterValues(driver.findElement(By.name("SocialSecurityNumber_1")),"354754664");

                enterValues(driver.findElement(By.name("Address1_1")),"123 AVE ST");

                enterValues(driver.findElement(By.name("zip_1")),"12345");

                enterValues(driver.findElement(By.name("percentOwned_1")),"30");
                clickonElement(driver.findElement(By.cssSelector(".link.add-owner1")));

                //Owner3


                enterValues(driver.findElement(By.name("name_2")),"JOHN BB");

                //  selectByVisibleText(driver.findElement(By.name("title1_2")),"Partner" );

                enterValues(driver.findElement(By.name("phone_2")),"3465265465");

                enterValues(driver.findElement(By.name("mobilePhone_2")),"4565365465");

                selectByVisibleText(driver.findElement(By.name("dob_month_2")),"March" );

                selectByVisibleText(driver.findElement(By.name("dob_day_2")),"5" );

                selectByVisibleText(driver.findElement(By.name("dob_year_2")),"1988" );


                enterValues(driver.findElement(By.name("SocialSecurityNumber_2")),"354754665");

                enterValues(driver.findElement(By.name("Address1_2")),"123 AVE ST");

                enterValues(driver.findElement(By.name("zip_2")),"12345");

                enterValues(driver.findElement(By.name("percentOwned_2")),"20");
                clickonElement(driver.findElement(By.cssSelector(".btn.submit")));
                wait.until(ExpectedConditions.urlContains("signup/location"));


                editDevicesLocation("FD150","Location 3");
                Thread.sleep(1000);
                //If you want to edit the location
                editDevicesLocation("Clover Flex 2nd Gen","Location 1");
                Thread.sleep(1000);


                //Location 1
                locationinfo(true,"1",true,"MULTI_PAY");
                WebElement next_location2 = driver.findElement(By.xpath("//a[text()='Next: Location 2' and not(contains(@class,'disabled')) ]"));
                clickonElement(next_location2);
                Thread.sleep(10000);
                WebElement locationcolor =driver.findElement(By.xpath("//a[text()='2']"));
                Assert.assertEquals(locationcolor.getCssValue("background-color"),"rgba(240, 61, 0, 1)");

                //location 2
                locationinfo(false,"2",false,"");
                WebElement next_location3 = driver.findElement(By.xpath("//a[text()='Next: Location 3' and not(contains(@class,'disabled')) ]"));
                clickUsingJs(next_location3);
                Thread.sleep(5000);
                // locationcolor =driver.findElement(By.xpath("//a[text()='3']"));
                //Assert.assertEquals(locationcolor.getCssValue("background-color"),"rgba(240, 61, 0, 1)");
                //location 3
                locationinfo(false,"3",false,"");
                Thread.sleep(2000);

                Thread.sleep(2000);
                WebElement next_location4 = driver.findElement(By.xpath("//a[text()='Next: Location 4' and not(contains(@class,'disabled')) ]"));
                clickUsingJs(next_location4);
                Thread.sleep(5000);
            //     locationcolor =driver.findElement(By.xpath("//a[text()='4']"));
             //   Assert.assertEquals(locationcolor.getCssValue("background-color"),"rgba(240, 61, 0, 1)");
                //location 4
                locationinfo(false,"4",true,"");
                WebElement next_location5 = driver.findElement(By.xpath("//a[text()='Next: Location 5' and not(contains(@class,'disabled')) ]"));
                clickUsingJs(next_location5);
                Thread.sleep(5000);
               //  locationcolor =driver.findElement(By.xpath("//a[text()='5']"));
                //Assert.assertEquals(locationcolor.getCssValue("background-color"),"rgba(240, 61, 0, 1)");
                //location 5
                locationinfo(false,"5",false,"");

                WebElement btncontinue = driver.findElement(By.xpath("//a[text()='Continue' and not(contains(@class,'disabled'))]"));
                clickonElement(btncontinue);
                clickonElement(driver.findElement(By.xpath("//a[contains(text(),'Approve') and not(contains(@class,'disabled'))]")));
                wait.until(ExpectedConditions.urlContains("signup/setup"));
                advancedSetup(true);
                wait.until(ExpectedConditions.urlContains("signup/terms"));
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='Continue' and @ng-click='showLeaseAgreement()']")));
                verifyMPADetails("Legal Business Name","MMIS TEST 31JAN2022 GFT");
                verifyMPADetails("ZIP","12345");
                List<WebElement> ownersize = driver.findElements(By.xpath("//h3[contains(text(),'Owner')]"));
                System.out.println(ownersize.size());
                verifyOwnerLocation("Location",5);
                verifyOwnerLocation("Owner",3);

                sendForRemoteSignature();





                /*
                WebElement agreementApproval = driver.findElement(By.xpath("//a[text()='AGREEMENT APPROVAL']"));
                clickonElement(agreementApproval);
                WebElement confirmation = driver.findElement(By.xpath("//a[text()='CONFIRMATION']"));
                clickonElement(confirmation);

                clickonElement(driver.findElement(By.xpath("//a[text()='Continue' and @ng-click='showLeaseAgreement()']")));
                wait.until(ExpectedConditions.urlContains("/signup/terms"));
                clickonElement(driver.findElement(By.linkText("SUBSCRIPTION")));
                clickonElement(driver.findElement(By.linkText("SUBSCRIPTION PERSONAL GUARANTEE")));
                */



            } catch (Exception | Error e) {
                e.printStackTrace();

                String scrBase64 = "data:image/png;base64," + ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);

                ExtentTestManager.getTest().log(Status.FAIL, e.getMessage(), ExtentTestManager.getTest().addScreenCaptureFromBase64String(scrBase64).getModel().getMedia().get(0));
            } finally {
                driver.close();
                driver.quit();
                ExtentManager.extentReports.flush();
            }
        }

    private static void verifyOwnerLocation(String fieldname,int expectedSize) {
            String xpath = "//h3[contains(text(),'"+fieldname+"')]";
            List<WebElement> actualSize = driver.findElements(By.xpath(xpath));
            if(actualSize.size()==expectedSize){
                ExtentTestManager.getTest().log(Status.INFO, fieldname+" ::matched with the provided value.");
            }else{
                ExtentTestManager.getTest().log(Status.WARNING, fieldname+" :: does not match with the provided value. Actaul Value is"+actualSize.size());
            }
    }

    private static void verifyMPADetails(String fieldname,String expectedValue) {
        String xpath = "//label[text()='"+fieldname+"']/following-sibling::span";
        //By text = RelativeLocator.with(By.tagName("span")).toRightOf(By.xpath(xpath));
        String text = getText(driver.findElement(By.xpath(xpath)));
        System.out.println(text);
        if(text.equals(expectedValue)){
            ExtentTestManager.getTest().log(Status.INFO, "Field Name::"+fieldname+" matched with the provided value.");
        }else{
            ExtentTestManager.getTest().log(Status.WARNING, "Field Name::"+fieldname+" does not match with the provided value. Value show is"+text);
        }

    }

    private static void enterPromotions(String promotion) throws InterruptedException {
           // By showDetail = RelativeLocator.with(By.xpath("//a[text()='Show Detail']")).toRightOf(By.xpath("//h3[text()='Promotions']"));
        JavascriptExecutor js = (JavascriptExecutor)driver;
        By showDetail = By.xpath("//h3[text()='Promotions']/ancestor::tr/th/a[text()='Show Detail']");
            /*clickonElement(driver.findElement(showDetail));
            Actions actions = new Actions(driver);
            actions.sendKeys(Keys.PAGE_DOWN);
            actions.moveToElement(driver.findElement(showDetail)).click();*/

            js.executeScript("arguments[0].click();", driver.findElement(showDetail));
            String xpath = "//td[text()='"+promotion+"']/following-sibling::td/a[text()=' Apply']";
            By applyPromotion = By.xpath(xpath);
            //By applyPromotion = RelativeLocator.with(By.xpath("//a[text()=' Apply']")).toRightOf(By.xpath(xpath));
            Thread.sleep(2000);
            js.executeScript("arguments[0].click();",driver.findElement(applyPromotion));
            //clickonElement(driver.findElement(applyPromotion));

            try{
                xpath = "//td[text()='"+promotion+"']/following-sibling::td/a[text()=' Remove']";
                WebElement element = driver.findElement(By.xpath(xpath));
                if(!element.isDisplayed()){

                    ExtentTestManager.getTest().log(Status.WARNING,"Promotion provided is not valid to apply"+promotion);
                }else{
                    ExtentTestManager.getTest().log(Status.INFO,"Promotion provided is valid to apply"+promotion);
                }
            }catch(Exception e){
                ExtentTestManager.getTest().log(Status.WARNING,"Promotion provided is not applied :: "+promotion);
            }
        }
        private static void editDevicesLocation(String product,String location) throws InterruptedException {
            JavascriptExecutor js = (JavascriptExecutor)driver;


        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(30));
           //



            wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[text()='Edit']"))));
            WebElement edit = driver.findElement(By.xpath("//a[text()='Edit']"));
            js.executeScript("arguments[0].click();", edit);
          //  edit.click();


            List<WebElement>elements = driver.findElements(By.xpath("//div[@id='product-configuration-options']//p"));
            wait.until(ExpectedConditions.visibilityOfAllElements((elements)));
            for(int i=0;i<elements.size();i++){
                if(elements.get(i).getText().equalsIgnoreCase(product)){
                    //By passwordLocator = RelativeLocator.with(By.tagName("input")).below(By.id("email"));
                    String xpath = "(//div[@id='product-configuration-options']//p[text()='"+product+"']/parent::div/div//select)[1]";
                    Thread.sleep(500);
                    WebElement select = driver.findElement(By.xpath(xpath));
                    selectByVisibleText(select,location);
                    break;
                }
            }
            Thread.sleep(500);
        WebElement save = driver.findElement(By.xpath("//div[@id='add-equipment']/div//a[text()='Save']"));
            clickonElement(save);


    }

    private static void locationinfo ( boolean isFirstLocation, String storeNumber,boolean isClover,String token) throws InterruptedException {
            // WebDriver driver
            enterValues(driver.findElement(By.name("storeNumber")), storeNumber);
            WebElement address = driver.findElement(By.name("business_address1"));
            WebElement zip = driver.findElement(By.name("business_address_zip"));
            WebElement email = driver.findElement(By.name("business_address_email"));
            WebElement email2 = driver.findElement(By.name("business_address_email2"));
            if (!isFirstLocation) {

                enterValues(address, "12 AVE ST");

                enterValues(zip, "12345");

                enterValues(email, "ashwin.sathyanarayanan@fiserv.com");

                enterValues(email2, "ashwin.sathyanarayanan@fiserv.com");
                WebElement attention = driver.findElement(By.id("attentionTo"));
                enterValues(attention, "Attention");
                selectByValue(driver.findElement(By.id("facetoface")), "100");
                clickonElement(driver.findElement(By.id("google-map-location")));
                Thread.sleep(2000);


            } else {
                if (!getAttribute(address, "value").isBlank() && !getAttribute(email, "value").isBlank() && !getAttribute(zip, "value").isBlank()
                        && !getText(driver.findElement(By.cssSelector("#mcctypes>option[selected='selected'][class]"))).isBlank()
                        && !getText(driver.findElement(By.cssSelector("#mcccodes>option[selected='selected'][class]"))).isBlank())
                    ExtentTestManager.getTest().log(Status.INFO, "Primary Location is prefilled with values");
                Thread.sleep(2000);
                //enterValues(driver.findElement(By.id("amexMemberId")), "9879867687");
                clickonElement(driver.findElement(By.id("facetoface")));
                {
                    WebElement facetoface = driver.findElement(By.id("facetoface"));
                    facetoface.findElement(By.xpath("//option[. = '100%']")).click();
                }
                Thread.sleep(2000);
                clickonElement(driver.findElement(By.id("google-map-location")));
                Thread.sleep(2000);

            }

            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

       /* selectByValue(driver.findElement(By.id("facetoface")),"100" );


        Thread.sleep(5000);
        clickonElement(driver.findElement(By.id("google-map-location")));
        Thread.sleep(5000);*/
            if(isClover) {

                if(!token.isBlank()){
                    WebElement configure = driver.findElement(By.xpath("//a[text()='Configure']"));
                   // Actions action = new Actions(driver);
                    //action.moveToElement(configure).click(configure);
                     clickonElement(configure);
                    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[normalize-space(text())='Configure Terminal']"))));
                    selectByVisibleText(driver.findElement(By.xpath("//select[@placeholder='TRANSARMOR TOKEN TYPE']")), token);
                    clickonElement(driver.findElement(By.xpath("//a[@ng-click='saveConfigureProduct(activeProduct)']")));
                }
                WebElement cloverSoftware = driver.findElement(By.xpath("//p[text()='Payments']/following-sibling::div/span/a"));
                clickonElement(cloverSoftware);
            }
            if (isFirstLocation) {
                WebElement abaNumber = driver.findElement(By.name("abaNumber"));
            /*
            enterValues(abaNumber, "021000021"+Keys.TAB);
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[text()='JPMORGAN CHASE BANK, NA']")));

             */
                for (int i = 0; i < 3; i++) {
                    enterValues(abaNumber, "021000021" + Keys.TAB);
                }
                Thread.sleep(2000);
                System.out.println(driver.findElement(By.xpath("//div[@class='form-element']/p[@ng-show='bankErrorServerFails']")).getText());
                //wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[text()='We're sorry, but we are unable to get bank details at this time. You will be contacted in future to provide the routing number.']")));
                WebElement accNumber = driver.findElement(By.name("ACCOUNT_NUMBER"));
                enterValues(accNumber, "123456");
                WebElement accNumber_reenter = driver.findElement(By.name("ACCOUNT_NUMBER_confirm"));
                enterValues(accNumber_reenter, "123456");
                /*
                //Add Bank Account 2
                clickonElement(driver.findElement(By.xpath("//strong[normalize-space(text())='Add bank account']")));
                clickonElement(driver.findElement(By.xpath("//input[@name='deposits' and @value='1']/following-sibling::div")));

                for (int i = 0; i < 3; i++) {
                    enterValues(abaNumber, "021000021" + Keys.TAB);
                }
                Thread.sleep(2000);
                System.out.println(driver.findElement(By.xpath("//div[@class='form-element']/p[@ng-show='bankErrorServerFails']")).getText());

                WebElement accNumber2 = driver.findElement(By.name("ACCOUNT_NUMBER"));
                enterValues(accNumber2, "456789");
                WebElement accNumber_reenter2 = driver.findElement(By.name("ACCOUNT_NUMBER_confirm"));
                enterValues(accNumber_reenter2, "456789");

                 */



            }
            Thread.sleep(2000);

            WebElement shippingAddress = driver.findElement(By.id("isShippingAddressSame"));
            selectByVisibleText(shippingAddress, "Yes");
        /*
        clickonElement(driver.findElement(By.id("isShippingAddressSame")));
        {
            WebElement shipAddress = driver.findElement(By.id("isShippingAddressSame"));
            shipAddress.findElement(By.xpath("//option[. = 'Yes']")).click();
        }
        */

           // wait.until(ExpectedConditions.presenceOfElementLocated(By.name("shippingOption")));
            Thread.sleep(3000);
            if (isFirstLocation) {

                // WebElement siteVisitation = driver.findElement(By.id("site-visitation"));
                // Thread.sleep(1000);
                //selectByVisibleText(siteVisitation, "Not Required");
                // Thread.sleep(1000);

           /* selectByIndex(driver.findElement(By.id("site-visitation")),1);
            Thread.sleep(2000);
            selectByIndex(driver.findElement(By.id("site-visitation")),1);*/


                selectByValue(driver.findElement(By.id("site-visitation")), "Not Required");

            /*clickonElement(driver.findElement(By.id("site-visitation")));
            {
                WebElement siteVisit = driver.findElement(By.id("site-visitation"));
               // selectByValue(driver.findElement(By.id("site-visitation")),"Not Required");
                Thread.sleep(1000);
               siteVisit.findElement(By.xpath("//option[. = 'Not Required']")).click();
                Thread.sleep(1000);
            }*/
            /*Actions action = new Actions(driver);
            action.sendKeys(Keys.CONTROL).sendKeys(Keys.HOME).build().perform();
            clickonElement(driver.findElement(By.id("facetoface")));
            {
                WebElement facetoface = driver.findElement(By.id("facetoface"));
                facetoface.findElement(By.xpath("//option[. = '100%']")).click();
            }


            Thread.sleep(5000);
            clickonElement(driver.findElement(By.id("google-map-location")));
            Thread.sleep(5000);*/

            }


            // selectByValue(driver.findElement(By.id("facetoface")),"100" );


        }

        private static void selectByVisibleText (WebElement element, String value){
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            wait.until(ExpectedConditions.elementToBeClickable(element));
            Select select = new Select(element);
            select.selectByVisibleText(value);
        }
        private static void selectByValue (WebElement element, String value){
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            wait.until(ExpectedConditions.elementToBeClickable(element));
            Select select = new Select(element);
            select.selectByValue(value);
        }
        private static void selectByIndex (WebElement element,int value){
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            wait.until(ExpectedConditions.elementToBeClickable(element));
            Select select = new Select(element);
            select.selectByIndex(value);
        }
        private static void selectByValue2 (WebElement element, String value){
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            wait.until(ExpectedConditions.elementToBeClickable(element));
            Select select = new Select(element);
            List<WebElement> elements = select.getOptions();
            for (WebElement ele : elements) {
                if (ele.getAttribute("value").equals(value)) {
                    ele.click();
                }
            }
        }
        private static void enterValues (WebElement element, String value){
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
            wait.until(ExpectedConditions.visibilityOf(element));
            wait.until(ExpectedConditions.elementToBeClickable(element));
            element.clear();
            element.sendKeys(value);
        }
        private static String getText (WebElement element){
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            wait.until(ExpectedConditions.visibilityOf(element));
            wait.until(ExpectedConditions.elementToBeClickable(element));

            return element.getText();
        }
        private static void clickonElement (WebElement element){
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            wait.until(ExpectedConditions.visibilityOf(element));
            wait.until(ExpectedConditions.elementToBeClickable(element));
            element.click();


        }
    private static void clickonElementStale (WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));

        boolean result = false;
        int attempts = 0;
        while(attempts < 2) {
            try {
                element.click();
                result = true;
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }

    }
        private static String getAttribute (WebElement element, String attr){
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            wait.until(ExpectedConditions.elementToBeClickable(element));
            return element.getAttribute(attr);
        }
        private static void addPaymentTypes(String paymenttype){
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            String ele = "//p[text()='"+paymenttype+"']/following-sibling::a[text()='Add to Cart']";
            WebElement element = driver.findElement(By.xpath(ele));
            wait.until(ExpectedConditions.visibilityOf(element));
            wait.until(ExpectedConditions.elementToBeClickable(element));
            element.click();
        }

        private static void selectProduct(String product) throws InterruptedException {
            Thread.sleep(200);
            clickonElement(driver.findElement(By.xpath("//div[@id='cart-actions']/div/a[text()='Continue Shopping']")));
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            enterValues(driver.findElement(By.id("search-products")),product);
            Thread.sleep(2000);
            String xpath = "//p[text()='"+product+"']/following-sibling::a[text()='View Details']";
            clickonElement(driver.findElement(By.xpath(xpath)));
            Thread.sleep(3000);
            clickUsingJs(driver.findElement(By.id("add-to-cart")));
        }

    private static void clickUsingJs(WebElement element) {

        JavascriptExecutor js = (JavascriptExecutor)driver;


        js.executeScript("arguments[0].click();", element);
    }

    private static void selectfromList (By by, String value){
            List<WebElement> elements = driver.findElements(by);
            for (WebElement element : elements) {
                if (elements.equals("100%")) {

                }
            }
        }
        private static void advancedSetup (boolean verifyMultiPay) throws InterruptedException {
            enterValues(driver.findElement(By.id("installContactName")), "JOHN DR");
            clickonElement(driver.findElement(By.id("anticipatedOpeningDate")));
            // Date date = new Date();
            // date.getDay();
            LocalDate date = LocalDate.now().plusDays(1);
            String day = "//td/a[text()='"+date.getDayOfMonth()+"']";
            ;
            clickonElement(driver.findElement(By.xpath(day)));

            enterValues(driver.findElement(By.id("installContactPhone")), "6548787878");
            if(verifyMultiPay){
                //selectByValue(driver.findElement(By.id("transarmorMultipayToken")),"Agent");
                //selectByValue(driver.findElement(By.id("transarmorMultipayToken")),"Outlet");

                Select select = new Select(driver.findElement(By.id("transarmorMultipayToken")));
                System.out.println(select.getFirstSelectedOption().getText());
                if(select.getFirstSelectedOption().getText().equalsIgnoreCase("Outlet")){
                    ExtentTestManager.getTest().log(Status.INFO, "Default value is Outlet for MULTI_PAY.");
                }else{
                    ExtentTestManager.getTest().log(Status.WARNING, "Default value is not Outlet for MULTI_PAY. Actaul value is "+select.getFirstSelectedOption().getText());
                }

                //System.out.println(getAttribute(driver.findElement(By.id("transarmorMultipayToken")),"value"));

            }
            clickonElement(driver.findElement(By.cssSelector(".btn.submit")));
            //Facetoface -Yes
            clickonElement(driver.findElement(By.cssSelector("input[value='Yes']")));
            clickonElement(driver.findElement(By.xpath("//a[text()='Next']")));//and not(contains(@class,'disabled'))
            Thread.sleep(500);
            clickonElement(driver.findElement(By.cssSelector("input[value='At the location of the business']")));
            clickonElement(driver.findElement(By.xpath("//a[text()='Next']")));// and not(contains(@class,'disabled'))
            Thread.sleep(500);
            clickonElement(driver.findElement(By.cssSelector("input[value='Remote Signature']")));
            clickonElement(driver.findElement(By.xpath("//a[text()='Proceed']")));// and not(contains(@class,'disabled'))
            Thread.sleep(5000);
            //clickonElement(driver.findElement(By.xpath("//button[text()='Continue']")));


        }
        public static void sendForRemoteSignature(){
            WebElement open = driver.findElement(By.id("sales-toggle"));
            clickonElement(open);
            WebElement sendForSign = driver.findElement(By.xpath("//a[text()='Send For Signature']"));
            clickonElement(sendForSign);
            WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
            wait.until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.xpath("//div[@class='sales-container ng-scope']/div/div/h3")),"Sent for Remote Signature"));
        }
        public static  void editPricesforProfitablityError() throws InterruptedException {
            List<WebElement> elements = driver.findElements(By.xpath("//tr[@ng-repeat='p in productPricing track by $index']/td//input[not(contains(@disabled,'disabled'))]"));
            List<WebElement> products = driver.findElements(By.xpath("//tr[@ng-repeat='p in productPricing track by $index']/td/ng-form/input[not(contains(@disabled,'disabled'))]/ancestor::td/preceding-sibling::td"));
            for(int i=0;i<elements.size();i++){
                String originalPrice = elements.get(i).getAttribute("value");
                String minPrice = elements.get(i).getAttribute("abs-min");
                String maxPrice = elements.get(i).getAttribute("abs-max");
                enterValues(elements.get(i),String.valueOf(Float.parseFloat(minPrice)-1));
                String error =getAttribute(driver.findElement(By.xpath("//tr[@ng-repeat='p in productPricing track by $index']/td/i")),"class");
                if(error.equalsIgnoreCase("absMinError error-indicator fa fa-lg fa-exclamation-circle ng-scope"))
                   // System.out.println("Error Message matches for Minimum value");
                    ExtentTestManager.getTest().log(Status.INFO,"Error Message matches for Minimum value"+products.get(i).getText());
                Thread.sleep(500);
                enterValues(elements.get(i),String.valueOf(Float.parseFloat(maxPrice)+1));
                error =getAttribute(driver.findElement(By.xpath("//tr[@ng-repeat='p in productPricing track by $index']/td/i")),"class");
                if(error.equalsIgnoreCase("absMaxError error-indicator fa fa-lg fa-exclamation-circle ng-scope"))
                    ExtentTestManager.getTest().log(Status.INFO,"Error Message matches for Maximum value"+products.get(i).getText());
                   // System.out.println("Error Message matches for Maximum value");
                Thread.sleep(200);
                enterValues(elements.get(i),originalPrice);

            }
        }
        public static void attachbase64Img(String log){
            String scrShot =  "data:image/png;base64," + ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);

            ExtentTestManager.getTest().log(Status.INFO,log,ExtentTestManager.getTest().addScreenCaptureFromBase64String(scrShot).getModel().getMedia().get(0));

        }
        public static String getOrderId(){
            driver = new Augmenter().augment(driver);
            DevTools devTools = ((HasDevTools) driver).getDevTools();
            //DevTools devTools = ((ChromeDriver) driver).getDevTools();
            //DevTools devTools =driver.getDevTools();
            devTools.createSession();
            devTools.send(Log.enable());
            devTools.send(Network.clearBrowserCache());
            devTools.send(Network.setCacheDisabled(true));
            final String[] res = {null};
            final RequestId[] requestIds = new RequestId[1];
            devTools.send(Network.enable(Optional.empty(), Optional.empty(), Optional.of(100000000)));
            devTools.addListener(Network.responseReceived(), responseReceived -> {


                requestIds[0] = responseReceived.getRequestId();
                String url = responseReceived.getResponse().getUrl();
                //System.out.println("Url:: "+url);
                int status = responseReceived.getResponse().getStatus();
                // System.out.println("Status:: "+status);
                String type = responseReceived.getType().toJson();
                //System.out.println("Type:: "+type);
                String headers = responseReceived.getResponse().getHeaders().toString();
                //System.out.println("Headers:: "+headers);
                String responseBody = devTools.send(Network.getResponseBody(requestIds[0])).getBody();
                if(url.equals("https://qa.sales.firstdata.com/v1/merchantorders"))
                    res[0] = responseBody.replace("{\"orderId\":\"","").replace("\"}","");
                    System.out.println("Response ::: "+res[0]);

            });

            return res[0];
        }

}

