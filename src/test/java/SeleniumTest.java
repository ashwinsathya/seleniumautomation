import com.aventstack.extentreports.Status;

import com.paulhammant.ngwebdriver.NgWebDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.*;
import org.testng.Assert;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SeleniumTest {
    public static WebDriver driver;
    public static final String URL = "https://sso-fdc-qa-Krushna.Padhy:81ee4bc2-646a-4c4e-9b79-47615678d3c5@ondemand.saucelabs.com:443/wd/hub";
    public static void main(String[] args) {

        try {
            WebDriverManager.chromedriver().setup();
            ChromeOptions browserOptions = new ChromeOptions();
            browserOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);

            browserOptions.setPlatformName("Windows 10");
            browserOptions.setBrowserVersion("latest");
            Map<String, Object> sauceOptions = new HashMap<>();
            //sauceOptions.put("screenResolution", "1024x768");
           //sauceOptions.put("username", System.getenv("SAUCE_USERNAME"));
            //sauceOptions.put("accessKey", System.getenv("SAUCE_ACCESS_KEY"));
            browserOptions.setCapability("sauce:options", sauceOptions);
            //driver = new ChromeDriver(browserOptions);
            java.net.URL url = new URL("https://sso-fdc-qa-Krushna.Padhy:81ee4bc2-646a-4c4e-9b79-47615678d3c5@ondemand.us-west-1.saucelabs.com:443/wd/hub");

            driver = new RemoteWebDriver(url, browserOptions);
            JavascriptExecutor js = (JavascriptExecutor) driver;
            NgWebDriver ngDriver = new NgWebDriver(js);
            //ngDriver.waitForAngularRequestsToFinish();
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
            driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(30));
            driver.manage().window().maximize();
            ExtentTestManager.startTest("Sales Create lead flow", "Firstdata sales create lead flow");
            driver.get("https://qa.sales.firstdata.com/#/");
            clickonElement(driver.findElement(By.id("login-button")));

            enterValues(driver.findElement(By.id("username")),"Mrktest01");
            enterValues(driver.findElement(By.id("password")),"Dec212021");
            clickonElement(driver.findElement(By.xpath("//a[@class='btn submit']")));
            WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(20));
            ExtentTestManager.getTest().log(Status.INFO,"Logged In Successfully");
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@id='sales-toggle']/span[text()='Open']")));
            clickonElement(driver.findElement(By.id("sales-toggle")));
            ExtentTestManager.getTest().log(Status.INFO,"Creating Lead");
            clickonElement(driver.findElement(By.xpath("//a[text()='Create Lead']")));
            enterValues(driver.findElement(By.name("zip")),"12345");
            enterValues(driver.findElement(By.name("company")),"MMIS TEST 17JAN2022");
            enterValues(driver.findElement(By.name("name")),"JOHN DOE");
            enterValues(driver.findElement(By.name("phone")),"4655468897");
            enterValues(driver.findElement(By.name("email")),"ashwin.sathyanarayanan@fiserv.com");
            enterValues(driver.findElement(By.name("address1")),"10,AVE ST");
            enterValues(driver.findElement(By.name("vol")),"9000");
            Thread.sleep(500);
           // driver.findElement(By.id("bank")).clear();
            enterValues(driver.findElement(By.id("bank")),"984971355883");

           // Thread.sleep(2000);

            clickonElement(driver.findElement(By.xpath("//li[text()='EMPS OA SPECIAL REF PROG/EMPS OA SPECIAL REF PROG/984971355883']")));
            clickonElement(driver.findElement(By.xpath("//a[text()='Create']")));
            ExtentTestManager.getTest().log(Status.INFO,"Lead Created successfully");
            System.out.println("Status is::"+getText(driver.findElement(By.cssSelector("#selectToBegin h3.float-left"))));
            clickonElement(driver.findElement(By.xpath("//a[text()='Shop Products']")));
            clickonElement(driver.findElement(By.id("sales-toggle")));
            wait.until(ExpectedConditions.urlContains("/home#shop-businesstypes"));
            clickonElement(driver.findElement(By.xpath("//a/span[text()='Retail']")));
            wait.until(ExpectedConditions.urlContains("home#shop-businesstypes"));
            clickonElement( driver.findElement(By.linkText("RETAIL (KEY ENTRY)")));
           // ngDriver.waitForAngularRequestsToFinish();
            Thread.sleep(1000);
            wait.until(ExpectedConditions.urlContains("products/c#shop-businesstypes"));
            clickonElement(driver.findElement(By.xpath("//p[text()='Clover Flex 2nd Gen']/following-sibling::a")));
            Thread.sleep(1000);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.breadcrumb-inner.container > ul>li:nth-of-type(3)")));
            clickonElement(driver.findElement(By.id("add-to-cart")));
            WebElement qty = driver.findElement(By.cssSelector("select[ng-model='p.qty']"));
            /*wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("select[ng-model='p.qty']")));

            Select select = new Select(qty);
            select.selectByVisibleText("5");*/
            selectByVisibleText(qty,"5");
            Thread.sleep(2000);
            //ngDriver.waitForAngularRequestsToFinish();
            clickonElement(driver.findElement(By.xpath("//a[text()='Browse Products']")));
            Thread.sleep(2000);
            //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='Early Termination Fee']/following-sibling::a[@ng-click='addToCart(p)']")));
            clickonElement(driver.findElement(By.xpath("//p[text()='Early Termination Fee']/following-sibling::a[@ng-click='addToCart(p)']")));
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@id='cart-items']/tbody/tr/td/div/p/a[text()='Early Termination Fee']")));
            //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='Browse Products']")));
            Thread.sleep(2000);
           // ngDriver.waitForAngularRequestsToFinish();
            clickonElement(driver.findElement(By.xpath("//a[text()='Browse Products']")));
            Thread.sleep(1000);
            wait.until(ExpectedConditions.urlContains("/options/Tiers"));
            //3-tier
            //wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("a[href='#/processing/10010']")));
            //clickonElement(driver.findElement(By.cssSelector("a[href='#/processing/10010']")));
            clickonElement(driver.findElement(By.xpath("//p[text()='3 Tier']/following-sibling::a")));
            wait.until(ExpectedConditions.urlContains("/processing"));

            //Thread.sleep(2000);
           // wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='Visa/Mastercard']/following-sibling::a")));
            clickonElement(driver.findElement(By.xpath("//p[text()='Visa/Mastercard']/following-sibling::a")));
          //  Thread.sleep(500);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@class='cart-items']/tbody/tr/td/p[contains(text(),'Visa/Mastercard')]")));
           // wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='Debit (3 Tier)']/following-sibling::a")));
            Thread.sleep(2000);
            clickonElement(driver.findElement(By.xpath("//p[text()='Debit (3 Tier)']/following-sibling::a")));
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@class='cart-items']/tbody/tr/td/p[contains(text(),'3 Tier')]")));
            Thread.sleep(2000);
            //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='cart-actions']/a[text()='PROCEED TO CHECKOUT']")));
            clickonElement(driver.findElement(By.xpath("//div[@id='cart-actions']/a[text()='PROCEED TO CHECKOUT']")));
            wait.until(ExpectedConditions.textToBe(By.xpath("//div[@id='opportunity-cart']/h2"),"Store Locations"));
            clickonElement(driver.findElement(By.xpath("//div[@id='opportunity-cart']/div/a[text()='3']")));
            clickonElement(driver.findElement(By.xpath("//div[@id='cart-actions']/a[text()='Next']")));
            //Transaction Info page
            wait.until(ExpectedConditions.urlContains("transaction/info"));

            selectByVisibleText(driver.findElement(By.id("mcccodes")), "Appliances, Electronics, Computers");
            //wait.until(ExpectedConditions.elementToBeClickable(By.id("mcctypes")));
            selectByVisibleText(driver.findElement(By.id("mcctypes")), "Camera and Photo Supply");
            enterValues(driver.findElement(By.name("sales")),"10000");
            enterValues(driver.findElement(By.name("annualcardVolume")),"9000");
            enterValues(driver.findElement(By.name("ticket")),"500");

            //Thread.sleep(2000);
            //Click on Next
            //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='cart-actions']/a[text()='Next']")));
            clickonElement(driver.findElement(By.xpath("//div[@id='cart-actions']/a[text()='Next' and not(contains(@class,'disabled'))]")));

            //Click on Place Order
          //  Thread.sleep(2000);
            wait.until(ExpectedConditions.urlContains("/shipping"));
            //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='proposal']/div/a[text()='Place Order']")));
            clickonElement(driver.findElement(By.xpath("//div[@id='proposal']/div/a[text()='Place Order']")));
            wait.until(ExpectedConditions.urlContains("/thankyou"));
            //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='Sign Up']")));
            clickonElement(driver.findElement(By.xpath("//a[text()='Sign Up']")));
            wait.until(ExpectedConditions.urlContains("/signup/owner"));
            //Owner & Business Info
           // Thread.sleep(2000);


            selectByVisibleText(driver.findElement(By.name("TAX_FILING_NAME_SAME_AS_BUSINESS_LEGAL_NAME")),"Yes" );

            selectByVisibleText(driver.findElement(By.id("files-taxes")),"By owner's Social Security Number" );

            selectByVisibleText(driver.findElement(By.name("YEAR_BUSINESS_STARTED")),"2022" );

            selectByVisibleText(driver.findElement(By.name("MONTH_BUSINESS_STARTED")),"January" );

            selectByVisibleText(driver.findElement(By.name("INCORPORATION_STATE")),"Alabama" );

            selectByVisibleText(driver.findElement(By.name("ORGANIZATION_TYPE")),"Partnerships" );

            enterValues(driver.findElement(By.name("EIN")),"546545646");

            selectByVisibleText(driver.findElement(By.name("FOREIGN_OWNERSHIP")),"None" );

            //Partner1

            selectByVisibleText(driver.findElement(By.name("title1_0")),"Partner" );
            enterValues(driver.findElement(By.name("phone_0")),"3465465465");


            enterValues(driver.findElement(By.name("mobilePhone_0")),"4565465465");

            selectByVisibleText(driver.findElement(By.name("dob_month_0")),"January" );

            selectByVisibleText(driver.findElement(By.name("dob_day_0")),"3" );

            selectByVisibleText(driver.findElement(By.name("dob_year_0")),"1989" );
            enterValues(driver.findElement(By.name("email2_0")),"ashwin.sathyanarayanan@fiserv.com");

            enterValues(driver.findElement(By.name("SocialSecurityNumber_0")),"354654664");

            enterValues(driver.findElement(By.name("Address1_0")),"12, AVE ST");

            enterValues(driver.findElement(By.name("zip_0")),"12345");

            enterValues(driver.findElement(By.name("percentOwned_0")),"50");
            clickonElement(driver.findElement(By.cssSelector(".link.add-owner1")));

            //Owner 2

            enterValues(driver.findElement(By.name("name_1")),"JOHN AA");

            //selectByVisibleText(driver.findElement(By.name("title1_1")),"Partner" );

           enterValues(driver.findElement(By.name("phone_1")),"3465965465");

            enterValues(driver.findElement(By.name("phone_1")),"4565065465");

            selectByVisibleText(driver.findElement(By.name("dob_month_1")),"February" );

            selectByVisibleText(driver.findElement(By.name("dob_day_1")),"3" );

            selectByVisibleText(driver.findElement(By.name("dob_year_1")),"1988" );


            enterValues(driver.findElement(By.name("SocialSecurityNumber_1")),"354754664");

            enterValues(driver.findElement(By.name("Address1_1")),"123 AVE ST");

            enterValues(driver.findElement(By.name("zip_1")),"12345");

            enterValues(driver.findElement(By.name("percentOwned_1")),"30");
            clickonElement(driver.findElement(By.cssSelector(".link.add-owner1")));
            
            //Owner3
            

            enterValues(driver.findElement(By.name("name_2")),"JOHN BB");

          //  selectByVisibleText(driver.findElement(By.name("title1_2")),"Partner" );

            enterValues(driver.findElement(By.name("phone_2")),"3465265465");

            enterValues(driver.findElement(By.name("mobilePhone_2")),"4565365465");

            selectByVisibleText(driver.findElement(By.name("dob_month_2")),"March" );

            selectByVisibleText(driver.findElement(By.name("dob_day_2")),"5" );

            selectByVisibleText(driver.findElement(By.name("dob_year_2")),"1988" );


            enterValues(driver.findElement(By.name("SocialSecurityNumber_2")),"354854664");

            enterValues(driver.findElement(By.name("Address1_2")),"123 AVE ST");

            enterValues(driver.findElement(By.name("zip_2")),"12345");

            enterValues(driver.findElement(By.name("percentOwned_2")),"20");
            clickonElement(driver.findElement(By.cssSelector(".btn.submit")));
            wait.until(ExpectedConditions.urlContains("signup/location"));
            //Thread.sleep(2000);
           // ngDriver.waitForAngularRequestsToFinish();
            locationinfo(true,"1");
            //Thread.sleep(5000);
           /* Robot robot = new Robot();*/
           Actions action = new Actions(driver);
          // action.sendKeys(Keys.CONTROL).sendKeys(Keys.END).build().perform();
            WebElement next_location2 = driver.findElement(By.xpath("//a[text()='Next: Location 2' and not(contains(@class,'disabled')) ]"));
            clickonElement(next_location2);
            Thread.sleep(10000);
            WebElement locationcolor =driver.findElement(By.xpath("//a[text()='2']"));

            Assert.assertEquals(locationcolor.getCssValue("background-color"),"rgba(240, 61, 0, 1)");
            locationinfo(false,"2");
            //action.sendKeys(Keys.CONTROL).sendKeys(Keys.END).build().perform();
            WebElement next_location3 = driver.findElement(By.xpath("//a[text()='Next: Location 3' and not(contains(@class,'disabled'))]"));
            clickonElement(next_location3);
            Thread.sleep(5000);
            WebElement locationcolor2 =driver.findElement(By.xpath("//a[text()='3']"));
            Assert.assertEquals(locationcolor2.getCssValue("background-color"),"rgba(240, 61, 0, 1)");
            locationinfo(false,"3");
            //action.sendKeys(Keys.CONTROL).sendKeys(Keys.END).build().perform();
            WebElement btncontinue = driver.findElement(By.xpath("//a[text()='Continue' and not(contains(@class,'disabled'))]"));
            clickonElement(btncontinue);
            clickonElement(driver.findElement(By.xpath("//a[contains(text(),'Approve') and not(contains(@class,'disabled'))]")));
            wait.until(ExpectedConditions.urlContains("signup/setup"));

            //Location info
            //location1

            advancedSetup();
        } catch (Exception |Error e) {
            e.printStackTrace();

            String scrBase64 =  "data:image/png;base64," + ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);

            ExtentTestManager.getTest().log(Status.FAIL,e.getMessage(),ExtentTestManager.getTest().addScreenCaptureFromBase64String(scrBase64).getModel().getMedia().get(0));
        } finally {
            driver.close();
            driver.quit();
            ExtentManager.extentReports.flush();
        }
        /*
        driver.get("https://qa.merchantselfboarding.com/franchise/ford/samlvalidation.html");
        driver.findElement(By.name("SAMLResponse")).sendKeys("PHNhbWwycDpSZXNwb25zZQ0KCXhtbG5zOnNhbWwycD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnByb3RvY29sIg0KCXhtbG5zOnNhbWwyPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXNzZXJ0aW9uIg0KICAgICAgICAgICAgICAgICBJRD0iXzRhMGQwMmE4LTY1NmEtNDZhYi1iZjdlLTZhOWEyODZhY2U0OCINCiAgICAgICAgICAgICAgICAgVmVyc2lvbj0iMi4wIg0KICAgICAgICAgICAgICAgICBJc3N1ZUluc3RhbnQ9IjIwMTktMDUtMDJUMjI6MzA6NDcuNTU5MTE3OFoiDQogICAgICAgICAgICAgICAgIERlc3RpbmF0aW9uPSJodHRwczovL3N0YWdlLmJ1eS5maXJzdGRhdGEuY29tL2ZyYW5jaGlzZS92MS9hdXRob3JpemUvZm9yZC91c2VyIj4gDQoJPHNhbWwyOklzc3Vlcj5odHRwOi8vd3d3LmZvcmQuY29tPC9zYW1sMjpJc3N1ZXI+IDwhLS0gVVJMIE9GIFRIRSAgSVNTVUVSKEluIHRoaXMgY2FzZSBpdHMgRk9SRCktLT4NCgk8U2lnbmF0dXJlDQoJCXhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIj4NCgkJPFNpZ25lZEluZm8+DQoJCQk8Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIgLz4NCgkJCTxTaWduYXR1cmVNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNyc2Etc2hhMjU2IiAvPg0KCQkJPFJlZmVyZW5jZSBVUkk9IiNfNGEwZDAyYTgtNjU2YS00NmFiLWJmN2UtNmE5YTI4NmFjZTQ4Ij4NCgkJCQk8VHJhbnNmb3Jtcz4NCgkJCQkJPFRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNlbnZlbG9wZWQtc2lnbmF0dXJlIiAvPg0KCQkJCQk8VHJhbnNmb3JtIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIiAvPg0KCQkJCTwvVHJhbnNmb3Jtcz4NCgkJCQk8RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxlbmMjc2hhMjU2IiAvPg0KCQkJCTxEaWdlc3RWYWx1ZT5SQlRlTU12Q1JRaVBBRmpuQnFZcVlCdE9NZ00wUFVvSUdydExzdnBSTlFNPTwvRGlnZXN0VmFsdWU+DQoJCQk8L1JlZmVyZW5jZT4NCgkJPC9TaWduZWRJbmZvPg0KCQk8U2lnbmF0dXJlVmFsdWU+DQpKWmhaVEV2aXpoMVExV0ZFc1BoeDRoZ0hZcXgzWVRUaTc2NEZWeUhhUTZBSVZ1K3E1dS9kdmJJUjlNTnd5aWNuZi9CbTE4NlFWbVdsMGhYbTkyRCtrQWVycGdVejMxRXJHT2RBek1SODIwdWErSEU0V3B1bTVrdEVSTHlZZXVoZVRoNjdhOE1FUFVnU0Qvc0dLKzBiMXMvaXQ5aGp6b1FSVGlpK1BLbVBHUzVEa0poTVE4aU50VW0zTktLTHBTRWJ4REM3NVNBVUdkTlNicjlEZzg4a1Y2RTBrYmFjU281aER6YUF3VXh4UUsybmh3R3pNVmJDSVhqdFpscGp5eDFpWlFSTTNpR0p4WUxDMWRJRWJUem1Sd1RnelIxT0V0aVdpNW1mQk9mMzF3S0I0UkZ6czhacVdmN0RFZUhEM1Q1VzJ0cXZSdmtQR2l3S0lKTXliUFJVdGc9PQ0KPC9TaWduYXR1cmVWYWx1ZT4NCgkJPEtleUluZm8+DQoJCQk8WDUwOURhdGE+DQoJCQkJPFg1MDlDZXJ0aWZpY2F0ZT4NCjwvWDUwOUNlcnRpZmljYXRlPiANCgkJCTwvWDUwOURhdGE+DQoJCTwvS2V5SW5mbz4NCgk8L1NpZ25hdHVyZT4NCgk8c2FtbDJwOlN0YXR1cz4NCgkJPHNhbWwycDpTdGF0dXNDb2RlIFZhbHVlPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6c3RhdHVzOlN1Y2Nlc3MiIC8+DQoJPC9zYW1sMnA6U3RhdHVzPg0KCTxBc3NlcnRpb24gSUQ9Il9jNjc3ODMyMS1mOWMwLTQyNWEtYWQwOC01MTgyODE4OTRhN2MiDQogICAgICAgICAgICAgSXNzdWVJbnN0YW50PSIyMDE5LTA1LTAyVDIyOjMwOjQ3LjU1OVoiDQogICAgICAgICAgICAgVmVyc2lvbj0iMi4wIg0KCQl4bWxucz0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmFzc2VydGlvbiI+DQoJCTxJc3N1ZXI+aHR0cDovL3d3dy5mb3JkLmNvbTwvSXNzdWVyPiA8IS0tIFVSTCBPRiBUSEUgIElTU1VFUihJbiB0aGlzIGNhc2UgaXRzIEZPUkQpLS0+DQoJCTxTdWJqZWN0Pg0KCQkJPE5hbWVJRCBGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjEuMTpuYW1laWQtZm9ybWF0OnVuc3BlY2lmaWVkIj5hMmI0OWQ0OC0zZDQ0LTRjNWQtODA2NS1mZTkyYzFmNzc3ODk8L05hbWVJRD4gDQoJCQk8U3ViamVjdENvbmZpcm1hdGlvbiBNZXRob2Q9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpjbTpiZWFyZXIiPg0KCQkJCTxTdWJqZWN0Q29uZmlybWF0aW9uRGF0YSBOb3RPbk9yQWZ0ZXI9IjIwMTktMDUtMDJUMjI6MzU6NDcuNTU5WiINCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlY2lwaWVudD0iaHR0cHM6Ly9zdGFnZS5idXkuZmlyc3RkYXRhLmNvbS9mcmFuY2hpc2UvdjEvYXV0aG9yaXplL2ZvcmQvdXNlciIgLz4gDQoJCQk8L1N1YmplY3RDb25maXJtYXRpb24+DQoJCTwvU3ViamVjdD4NCgkJPENvbmRpdGlvbnMgTm90QmVmb3JlPSIyMDE5LTA1LTAyVDIyOjMwOjQ3LjU1OVoiDQogICAgICAgICAgICAgICAgTm90T25PckFmdGVyPSIyMDE5LTA1LTAyVDIzOjMwOjQ3LjU1OVoiPg0KCQkJPEF1ZGllbmNlUmVzdHJpY3Rpb24+DQoJCQkJPEF1ZGllbmNlPmh0dHBzOi8vc3RhZ2UuYnV5LmZpcnN0ZGF0YS5jb20vZnJhbmNoaXNlL3YxL2F1dGhvcml6ZS9mb3JkL3VzZXI8L0F1ZGllbmNlPg0KCQkJCQ0KCQkJPC9BdWRpZW5jZVJlc3RyaWN0aW9uPg0KCQk8L0NvbmRpdGlvbnM+DQoJCTxBdHRyaWJ1dGVTdGF0ZW1lbnQ+DQoJCQk8QXR0cmlidXRlIE5hbWU9InVzZXJJZCI+IA0KCQkJCTxBdHRyaWJ1dGVWYWx1ZT5hMmI0OWQ0OC0zZDQ0LTRjNWQtODA2NS1mZTkyYzFmNzc3ODk8L0F0dHJpYnV0ZVZhbHVlPiANCgkJCTwvQXR0cmlidXRlPg0KCQkJPEF0dHJpYnV0ZSBOYW1lPSJjb25zdW1lcklkIj4gDQoJCQkJPEF0dHJpYnV0ZVZhbHVlPmEyYjQ5ZDQ4LTNkNDQtNGM1ZC04MDY1LWZlOTJjMWY3Nzc4OTwvQXR0cmlidXRlVmFsdWU+IA0KCQkJPC9BdHRyaWJ1dGU+DQoJCQk8QXR0cmlidXRlIE5hbWU9InJlZGlyZWN0VXJsIj4NCgkJCQk8QXR0cmlidXRlVmFsdWU+aHR0cDovL3d3dy5mb3JkLmNvbTwvQXR0cmlidXRlVmFsdWU+IDwhLS1UZXN0IFVSTCBmb3IgRk9SRCAtLT4NCgkJCTwvQXR0cmlidXRlPg0KCQkJPEF0dHJpYnV0ZSBOYW1lPSJzdG9yZU51bWJlciI+DQoJCQkJPEF0dHJpYnV0ZVZhbHVlPkZEVEVTVDwvQXR0cmlidXRlVmFsdWU+IA0KCQkJPC9BdHRyaWJ1dGU+DQoJCQk8QXR0cmlidXRlIE5hbWU9ImNvbXBhbnlJZCI+DQoJCQkJPEF0dHJpYnV0ZVZhbHVlPmVtVnBCUFpyVzc8L0F0dHJpYnV0ZVZhbHVlPiANCgkJCTwvQXR0cmlidXRlPg0KCQk8L0F0dHJpYnV0ZVN0YXRlbWVudD4NCgkJPEF1dGhuU3RhdGVtZW50IEF1dGhuSW5zdGFudD0iMjAxOS0wNS0wMlQyMjozMDo0Ny41NTlaIj4NCgkJCTxBdXRobkNvbnRleHQ+DQoJCQkJPEF1dGhuQ29udGV4dENsYXNzUmVmPnVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphYzpjbGFzc2VzOlBhc3N3b3JkUHJvdGVjdGVkVHJhbnNwb3J0PC9BdXRobkNvbnRleHRDbGFzc1JlZj4NCgkJCTwvQXV0aG5Db250ZXh0Pg0KCQk8L0F1dGhuU3RhdGVtZW50Pg0KCTwvQXNzZXJ0aW9uPg0KPC9zYW1sMnA6UmVzcG9uc2U+");
        driver.findElement(By.tagName("button")).click();*/
    }
    private static void locationinfo(boolean isFirstLocation,String storeNumber) throws InterruptedException {
        // WebDriver driver
        enterValues(driver.findElement(By.name("storeNumber")), storeNumber);
        WebElement address = driver.findElement(By.name("business_address1"));
        WebElement zip = driver.findElement(By.name("business_address_zip"));
        WebElement email = driver.findElement(By.name("business_address_email"));
        WebElement email2 = driver.findElement(By.name("business_address_email2"));
        if (!isFirstLocation) {

            enterValues(address, "12 AVE ST");

            enterValues(zip, "12345");

            enterValues(email, "ashwin.sathyanarayanan@fiserv.com");

            enterValues(email2, "ashwin.sathyanarayanan@fiserv.com");
            WebElement attention = driver.findElement(By.id("attentionTo"));
            enterValues(attention, "Attention");
            selectByValue(driver.findElement(By.id("facetoface")), "100");
            clickonElement(driver.findElement(By.id("google-map-location")));
            Thread.sleep(2000);


        } else {
            if (!getAttribute(address, "value").isBlank() && !getAttribute(email, "value").isBlank() && !getAttribute(zip, "value").isBlank()
                    && !getText(driver.findElement(By.cssSelector("#mcctypes>option[selected='selected'][class]"))).isBlank()
                    && !getText(driver.findElement(By.cssSelector("#mcccodes>option[selected='selected'][class]"))).isBlank())
                ExtentTestManager.getTest().log(Status.INFO, "Primary Location is prefilled with values");
            Thread.sleep(5000);
            clickonElement(driver.findElement(By.id("facetoface")));
            {
                WebElement facetoface = driver.findElement(By.id("facetoface"));
                facetoface.findElement(By.xpath("//option[. = '100%']")).click();
            }
            Thread.sleep(5000);
            clickonElement(driver.findElement(By.id("google-map-location")));
            Thread.sleep(2000);

        }

        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));

       /* selectByValue(driver.findElement(By.id("facetoface")),"100" );


        Thread.sleep(5000);
        clickonElement(driver.findElement(By.id("google-map-location")));
        Thread.sleep(5000);*/
        WebElement cloverSoftware = driver.findElement(By.xpath("//p[text()='Payments']/following-sibling::div/span/a"));
        clickonElement(cloverSoftware);
        if(isFirstLocation){
            WebElement abaNumber = driver.findElement(By.name("abaNumber"));
            /*
            enterValues(abaNumber, "021000021"+Keys.TAB);
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[text()='JPMORGAN CHASE BANK, NA']")));

             */
            for(int i=0;i<3;i++) {
                enterValues(abaNumber, "021000021" + Keys.TAB);
            }
            Thread.sleep(2000);
            System.out.println(driver.findElement(By.xpath("//div[@class='form-element']/p[@ng-show='bankErrorServerFails']")).getText());
            //wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[text()='We're sorry, but we are unable to get bank details at this time. You will be contacted in future to provide the routing number.']")));
            WebElement accNumber = driver.findElement(By.name("ACCOUNT_NUMBER"));
            enterValues(accNumber, "123456");
            WebElement accNumber_reenter = driver.findElement(By.name("ACCOUNT_NUMBER_confirm"));
            enterValues(accNumber_reenter, "123456");

        }
        Thread.sleep(2000);

        WebElement shippingAddress = driver.findElement(By.id("isShippingAddressSame"));
        selectByVisibleText(shippingAddress,"Yes");
        /*
        clickonElement(driver.findElement(By.id("isShippingAddressSame")));
        {
            WebElement shipAddress = driver.findElement(By.id("isShippingAddressSame"));
            shipAddress.findElement(By.xpath("//option[. = 'Yes']")).click();
        }
        */

        wait.until(ExpectedConditions.presenceOfElementLocated(By.name("shippingOption")));
        Thread.sleep(2000);
        if(isFirstLocation) {

           // WebElement siteVisitation = driver.findElement(By.id("site-visitation"));
           // Thread.sleep(1000);
            //selectByVisibleText(siteVisitation, "Not Required");
           // Thread.sleep(1000);

           /* selectByIndex(driver.findElement(By.id("site-visitation")),1);
            Thread.sleep(2000);
            selectByIndex(driver.findElement(By.id("site-visitation")),1);*/


            selectByValue(driver.findElement(By.id("site-visitation")),"Not Required");

            /*clickonElement(driver.findElement(By.id("site-visitation")));
            {
                WebElement siteVisit = driver.findElement(By.id("site-visitation"));
               // selectByValue(driver.findElement(By.id("site-visitation")),"Not Required");
                Thread.sleep(1000);
               siteVisit.findElement(By.xpath("//option[. = 'Not Required']")).click();
                Thread.sleep(1000);
            }*/
            /*Actions action = new Actions(driver);
            action.sendKeys(Keys.CONTROL).sendKeys(Keys.HOME).build().perform();
            clickonElement(driver.findElement(By.id("facetoface")));
            {
                WebElement facetoface = driver.findElement(By.id("facetoface"));
                facetoface.findElement(By.xpath("//option[. = '100%']")).click();
            }


            Thread.sleep(5000);
            clickonElement(driver.findElement(By.id("google-map-location")));
            Thread.sleep(5000);*/

        }


       // selectByValue(driver.findElement(By.id("facetoface")),"100" );









    }

    private static void selectByVisibleText(WebElement element, String value){
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        Select select = new Select(element);
        select.selectByVisibleText(value);
    }
    private static void selectByValue(WebElement element, String value){
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        Select select = new Select(element);
        select.selectByValue(value);
    }
    private static void selectByIndex(WebElement element, int value){
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        Select select = new Select(element);
        select.selectByIndex(value);
    }
    private static void selectByValue2(WebElement element, String value){
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        Select select = new Select(element);
        List<WebElement>elements =select.getOptions();
        for(WebElement ele:elements){
            if(ele.getAttribute("value").equals(value)){
                ele.click();
            }
        }
    }
    private static void enterValues(WebElement element,String value){
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.clear();
        element.sendKeys(value);
    }
    private static String getText(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));

        return element.getText();
    }
    private static void clickonElement(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }
    private static String getAttribute(WebElement element,String attr){
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        return element.getAttribute(attr);
    }
    private static void selectfromList(By by,String value){
        List<WebElement> elements = driver.findElements(by);
        for(WebElement element:elements){
            if(elements.equals("100%")){

            }
        }
    }
    private static void advancedSetup() throws InterruptedException {
        enterValues(driver.findElement(By.id("installContactName")), "JOHN DR");
        clickonElement(driver.findElement(By.id("anticipatedOpeningDate")));
       // Date date = new Date();
       // date.getDay();
        clickonElement(driver.findElement(By.xpath("//td/a[text()='20']")));

        enterValues(driver.findElement(By.id("installContactPhone")), "6548787878");
        clickonElement(driver.findElement(By.cssSelector(".btn.submit")));
        clickonElement(driver.findElement(By.cssSelector("input[value='Yes']")));
        clickonElement(driver.findElement(By.xpath("//a[text()='Next']")));//and not(contains(@class,'disabled'))
        Thread.sleep(500);
        clickonElement(driver.findElement(By.cssSelector("input[value='At the location of the business']")));
        clickonElement(driver.findElement(By.xpath("//a[text()='Next']")));// and not(contains(@class,'disabled'))
        Thread.sleep(500);
        clickonElement(driver.findElement(By.cssSelector("input[value='Face to Face(On Screen)']")));
        clickonElement(driver.findElement(By.xpath("//a[text()='Proceed']")));// and not(contains(@class,'disabled'))
        Thread.sleep(5000);
        clickonElement(driver.findElement(By.xpath("//button[text()='Continue']")));


    }

}
