import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ESign {
    public static ChromeDriver driver;

    public static void main(String[] args) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        driver.get("https://qa.sales.firstdata.com/#/signup/terms/YAxpz/K4Eod");

        driver.findElement(By.xpath("//label[contains(.,'I agree to electronic records and e-signature.')]")).click();
        driver.findElement(By.id("esignature-optin-button")).click();
        driver.findElement(By.linkText("AGREEMENT APPROVAL")).click();
        driver.findElement(By.id("type-signature-1")).click();
        driver.findElement(By.id("type-signature-1")).sendKeys("Test");
        driver.findElement(By.linkText("CONFIRMATION")).click();
        driver.findElement(By.id("type-signature-2")).click();
        driver.findElement(By.id("type-signature-2")).sendKeys("Test");
        driver.findElement(By.cssSelector(".container:nth-child(17) .ng-scope")).click();
        driver.findElement(By.id("lease-agreement")).click();
        driver.findElement(By.linkText("SUBSCRIPTION")).click();
        driver.findElement(By.id("type-signature-3")).click();
        driver.findElement(By.id("type-signature-3")).sendKeys("Test");
        driver.findElement(By.id("type-signature-4")).click();
        driver.findElement(By.id("type-signature-4")).sendKeys("Test");
        Thread.sleep(5000);
        driver.findElement(By.xpath("//div[@id='lease-agreement']//a[text()='Submit']")).click();
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(20));
        wait.until(ExpectedConditions.urlContains("thankyou"));
        Thread.sleep(20000);



    }
}
