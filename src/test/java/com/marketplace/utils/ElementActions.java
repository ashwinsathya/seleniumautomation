package com.marketplace.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ElementActions {
    WebDriver driver;
    public WebElement getElement(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        return element;

    }
    public WebElement getDropDownElement(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
        wait.until(ExpectedConditions.elementToBeSelected(element));
        return element;
    }
    public void clickonElement(WebElement element){
        getElement(element).click();
    }
    public void enterValues(WebElement element,String value){
        getElement(element).sendKeys(value);
    }

}
