package com.marketplace.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashboardPage {
    WebDriver driver;
    public DashboardPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }
    @FindBy(id="sales-toggle")
    private WebElement btnopenClose;
    @FindBy(xpath="//a[text()='Create Lead']")
    private WebElement createLead;
    @FindBy(name="zip")
    private WebElement zip;
    @FindBy(name="company")
    private WebElement company;
    @FindBy(name="name")
    private WebElement name;
    @FindBy(name="phone")
    private WebElement phone;
    @FindBy(name="email")
    private WebElement email;
    @FindBy(name="address1")
    private WebElement address1;
    @FindBy(name="vol")
    private WebElement volume;
    @FindBy(id="bank")
    private WebElement bank;
    @FindBy(xpath="//a[text()='Create']")
    private WebElement create;

    public void setAddress1(String address1) {

    }
    public void createLead(){

    }
}
