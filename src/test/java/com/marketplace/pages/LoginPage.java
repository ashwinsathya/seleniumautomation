package com.marketplace.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class LoginPage {
    WebDriver driver;
    public LoginPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }
   @FindBy(id="username")
    private WebElement userName;
    @FindBy(id="password")
    private WebElement password;
    @FindBy(css="a[onclick='postOk();']")
    private WebElement login;

   public void loginWithCredentials(String user,String pwd){
       WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
       wait.until(ExpectedConditions.elementToBeClickable(userName));
       userName.sendKeys(user);
       wait.until(ExpectedConditions.elementToBeClickable(password));
       password.sendKeys(pwd);
       wait.until(ExpectedConditions.elementToBeClickable(login));
       login.click();

   }
}
