##### Steps to import the Project and run code
-  Open IntelliJ IDE -> Click on Get From VCS button
-  Provide the Git link "git clone git@bitbucket.org:ashwinsathya/seleniumautomation.git"
-  Provide the Directory where it needs to be imported
-  After it is downloaded, it takes time to download the gradle libraries in the IDE
-  Once this is done ,go to src/test/java/GFTFlow.java, click on Play button near to public static void main and run the scripts
- To view the reports, Go to extent-reports folder and click on the extent-report.html file in browser.

